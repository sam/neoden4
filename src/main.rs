#[macro_use]
extern crate serde_derive;

mod neoden4;
mod xpcb;

use clap::{ArgAction, Parser};
use color_eyre::eyre::{bail, ensure, eyre, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{BTreeSet, HashSet};
use std::fs::File;
use std::path::PathBuf;

#[derive(Parser)]
struct Options {
    /// Sets the PicknPlace.txt file to use
    input: PathBuf,
    /// Sets the X position of the reference component
    pos_x: f32,
    /// Set the Y position of the reference component
    pos_y: f32,
    #[clap(long = "ref", short = 'R', value_name = "NAME")]
    /// Sets the reference component (default: the first one)
    reference: Option<String>,
    #[clap(long, short, value_name = "ORDER")]
    /// Reorders components according to criteria (alpha, closest, topo)
    reorder: Option<String>,
    #[clap(long, short, value_name = "FILE")]
    /// Output file with Neoden 4 PnP format (default: standard output)
    output: Option<PathBuf>,
    #[clap(long, short = 'F', value_name = "FILE")]
    /// Name of the Fiducials.txt file (default: no fiducials)
    fiducials: Option<PathBuf>,
    #[clap(long, short)]
    /// Filters by PCB side (e.g., TOP)
    side: Option<String>,
    #[clap(long, value_name = "REGEX")]
    /// Filters by component name (regular expression)
    filter: Option<String>,
    /// Increase verbosity
    #[clap(long, short, action = ArgAction::Count)]
    verbose: usize,
}

/// A component to be placed on the board
#[derive(Clone, Deserialize)]
pub struct Component {
    designator: String,
    x: f32,
    y: f32,
    rotation: f32,
    value: String,
    package: String,
    side: String,
}

impl Component {
    /// Distance in microns between two components.
    pub fn distance(&self, other: &Component) -> u32 {
        ((self.x - other.x).hypot(self.y - other.y) * 1000.0) as u32
    }
}

/// A fiducial used as an anchor point for the PnP machine.
#[derive(Deserialize)]
pub struct Fiducial {
    designator: String,
    x: f32,
    y: f32,
    _rot: f32,
    _size: String,
    _package: String,
    side: String,
}

/// Decompose a component into a kind and an index, or a kind only
/// if it does not match one or more letters followed by one or more numbers.
///
/// "D17" -> ("D", 17)
/// "DIODE" -> ("DIODE", 0)
/// "47" -> ("47", 0)
/// "U1C12" -> ("U1C12", 0)
fn component_name(designator: &str) -> (String, u32) {
    lazy_static! {
        static ref COMPONENT_NAME: Regex = Regex::new(r"^([[:alpha:]]+)(\d+)$").unwrap();
    }
    COMPONENT_NAME
        .captures(designator)
        .and_then(|m| {
            Some((
                m.get(1).unwrap().as_str().to_owned(),
                m.get(2).unwrap().as_str().parse::<u32>().ok()?,
            ))
        })
        .unwrap_or_else(|| (designator.to_owned(), 0))
}

/// Reorder components by lexical order. If a reference component name is
/// passed, its kind will get put in front of the others.
fn reorder_components_alpha(components: &[Component], ref_name: Option<&str>) -> Vec<Component> {
    let reference = ref_name.map(component_name);
    let mut components = components
        .iter()
        .cloned()
        .map(|c| (component_name(&c.designator), c))
        .collect::<Vec<_>>();
    components.sort_unstable_by_key(|(k, _)| k.clone());
    if let Some((refk, _)) = reference {
        let (same_ref, different_ref): (Vec<_>, Vec<_>) =
            components.into_iter().partition(|((k, _), _)| k == &refk);
        components = same_ref;
        components.extend(different_ref);
    }
    components.into_iter().map(|(_, c)| c).collect::<Vec<_>>()
}

/// Reorder components by a chain of the closest non-place components.
/// The reference component name is used to find the starting point if
/// any, otherwise the first component is considered. Return an error
/// if the refererence component cannot bn located.
fn reorder_components_closest(
    components: &[Component],
    ref_name: Option<&str>,
) -> Result<Vec<Component>> {
    let mut rest = (0..components.len()).collect::<BTreeSet<_>>();
    let mut previous = ref_name
        .map(|n| locate_reference(components, n))
        .unwrap_or(Ok(0))?;
    let mut r = Vec::with_capacity(components.len());
    while let Some(&current) = rest
        .iter()
        .min_by_key(|&&i| components[previous].distance(&components[i]))
    {
        r.push(components[current].clone());
        rest.remove(&current);
        previous = current;
    }
    Ok(r)
}

/// Reorder components by topological order: the lowest components are
/// placed first, from left to right.
fn reorder_components_topo(components: &[Component]) -> Vec<Component> {
    let mut components = components.to_vec();
    components.sort_unstable_by_key(|c| ((c.y * 1000.0) as i32, (c.x * 1000.0) as i32));
    components
}

/// Only keep components pertaining to one side of the board (case insensitive).
/// Return an error if there are no component remaining.
fn filter_components_side(components: &[Component], side: &str) -> Result<Vec<Component>> {
    let sides = components
        .iter()
        .map(|c| c.side.to_lowercase())
        .collect::<HashSet<_>>();
    if !sides.contains(&side.to_lowercase()) {
        let mut sides = sides.into_iter().collect::<Vec<_>>();
        sides.sort();
        bail!(
            "side {} does not correspond to an actual PCB side ({})",
            side,
            sides.join(" / ")
        );
    }
    Ok(components
        .iter()
        .filter(|c| c.side.eq_ignore_ascii_case(side))
        .cloned()
        .collect())
}

/// Only keep components matching the given regular expression. Return an error if
/// the regular expression is invalid, or if no components are kept after the filter is run.
fn filter_components_name(components: &[Component], regex: &str) -> Result<Vec<Component>> {
    let filter = Regex::new(regex)
        .wrap_err_with(|| format!("unable to compile filter expression {}", regex))?;
    let components: Vec<_> = components
        .iter()
        .filter(|c| filter.is_match(&c.designator))
        .cloned()
        .collect();
    ensure!(!components.is_empty(), "no component pass filter {}", regex);
    Ok(components)
}

/// Locate a component given a reference component name and return its index or an error
/// if it cannot be found or if it is not unique. A name consisting of a kind without a
/// index will locate the first component of this kind.
fn locate_reference(components: &[Component], ref_name: &str) -> Result<usize> {
    let (refk, refn) = component_name(ref_name);
    let mut indices = (0..components.len()).filter(|&i| {
        let (ck, cn) = component_name(&components[i].designator);
        ck == refk && (cn == refn || refn == 0)
    });
    match (indices.next(), indices.next().is_some()) {
        (Some(_), true) if refn != 0 => Err(eyre!(
            "{} components are named {}, cannot use as reference",
            indices.count() + 2,
            ref_name
        )),
        (Some(i), _) => Ok(i),
        (None, _) => Err(eyre!(
            "no component named {}, cannot use as reference",
            ref_name
        )),
    }
}

/// Make sure that the reference component is placed first, the others being unaltered.
/// Return an error if the reference component cannot be located.
fn prepend_reference(components: &[Component], ref_name: &str) -> Result<Vec<Component>> {
    let i = locate_reference(components, ref_name)?;
    let mut r = vec![components[i].clone()];
    r.extend(components[..i].to_vec());
    r.extend(components[i + 1..].to_vec());
    Ok(r)
}

/// Run the program and return the error if any.
fn main() -> Result<()> {
    color_eyre::install()?;
    let opts = Options::parse();
    let mut components = xpcb::read_components(&opts.input)
        .wrap_err_with(|| format!("cannot read input file {:?}", opts.input))?;
    let mut fiducials = match opts.fiducials {
        Some(f) => xpcb::read_fiducials(&f)
            .wrap_err_with(|| format!("cannot read fiducials file {:?}", f))?,
        None => vec![],
    };
    if opts.verbose >= 1 {
        println!(
            "Number of components read from input file: {}",
            components.len()
        );
    }
    ensure!(
        !components.is_empty(),
        "empty components list: unable to use a reference"
    );
    if let Some(side) = opts.side {
        components = filter_components_side(&components, &side)?;
        fiducials.retain(|f| f.side.eq_ignore_ascii_case(&side));
    }
    if let Some(regex) = opts.filter {
        components = filter_components_name(&components, &regex)?;
    }
    match opts.reorder.as_deref() {
        Some("alpha") => {
            components = reorder_components_alpha(&components, opts.reference.as_deref())
        }
        Some("closest") => {
            components = reorder_components_closest(&components, opts.reference.as_deref())?
        }
        Some("topo") => components = reorder_components_topo(&components),
        Some(order) => bail!("unknown sorting method {}", order),
        None => (),
    }
    if let Some(reference) = opts.reference {
        components = prepend_reference(&components, &reference)?;
    }
    if opts.verbose >= 1 {
        println!("Reference component: {}", components[0].designator);
    }
    match opts.output {
        Some(path) => neoden4::write_components(
            &mut File::create(&path)?,
            &components,
            opts.pos_x,
            opts.pos_y,
        )
        .wrap_err_with(|| format!("unable to write components file {:?}", path))?,
        None => neoden4::write_components(
            &mut std::io::stdout().lock(),
            &components,
            opts.pos_x,
            opts.pos_y,
        )
        .wrap_err("unable to write components list to standard output")?,
    }
    if !fiducials.is_empty() {
        neoden4::print_fiducials(&fiducials, &components, opts.pos_x, opts.pos_y);
    }
    Ok(())
}

#[cfg(test)]
mod tests {

    use super::*;

    fn components() -> Vec<Component> {
        xpcb::read_components("tests/PicknPlace.txt").unwrap()
    }

    #[test]
    fn test_component_name() {
        assert_eq!(component_name("D17"), ("D".to_owned(), 17));
        assert_eq!(component_name("DIODE"), ("DIODE".to_owned(), 0));
        assert_eq!(component_name("R2D2"), ("R2D2".to_owned(), 0));
    }

    #[test]
    fn test_reorder_alpha() {
        let components = components();

        let sorted = reorder_components_alpha(&components, None);
        assert_eq!(components.len(), sorted.len());
        assert_eq!(sorted[0].designator, "C1");
        assert_eq!(sorted[1].designator, "C2");

        let sorted = reorder_components_alpha(&components, Some("D1"));
        assert_eq!(components.len(), sorted.len());
        assert_eq!(sorted[0].designator, "D1");
        assert_eq!(sorted[1].designator, "D2");

        let sorted = reorder_components_alpha(&components, Some("D"));
        assert_eq!(components.len(), sorted.len());
        assert_eq!(sorted[0].designator, "D1");
        assert_eq!(sorted[1].designator, "D2");

        let sorted = reorder_components_alpha(&components, Some("NONEXISTENT"));
        assert_eq!(components.len(), sorted.len());
        assert_eq!(sorted[0].designator, "C1");
        assert_eq!(sorted[1].designator, "C2");
    }

    #[test]
    fn test_reorder_closest() {
        let components = filter_components_side(&components(), "BOTTOM").unwrap();
        let components = filter_components_name(&components, r"^[CR]\d+").unwrap();

        let sorted = reorder_components_closest(&components, None)
            .unwrap()
            .into_iter()
            .map(|c| c.designator)
            .collect::<Vec<_>>();
        assert_eq!(sorted, vec!["C1", "R1", "R2", "C4", "C5", "C3", "C2"]);

        let sorted = reorder_components_closest(&components, Some("C1"))
            .unwrap()
            .into_iter()
            .map(|c| c.designator)
            .collect::<Vec<_>>();
        assert_eq!(sorted, vec!["C1", "R1", "R2", "C4", "C5", "C3", "C2"]);

        let sorted = reorder_components_closest(&components, Some("C"))
            .unwrap()
            .into_iter()
            .map(|c| c.designator)
            .collect::<Vec<_>>();
        assert_eq!(sorted, vec!["C1", "R1", "R2", "C4", "C5", "C3", "C2"]);

        let sorted = reorder_components_closest(&components, Some("C2"))
            .unwrap()
            .into_iter()
            .map(|c| c.designator)
            .collect::<Vec<_>>();
        assert_eq!(sorted, vec!["C2", "C3", "C1", "R1", "R2", "C4", "C5"]);
    }

    #[test]
    fn test_reorder_topo() {
        let components = components();

        let sorted = reorder_components_topo(&components);
        assert_eq!(components.len(), sorted.len());
        assert_eq!(sorted[0].designator, "D1");
    }

    #[test]
    fn test_filter_side() {
        let components = components();

        let top = filter_components_side(&components, "TOP").unwrap();
        let bottom = filter_components_side(&components, "BOTTOM").unwrap();
        assert_eq!(top.len() + bottom.len(), components.len());

        let top2 = filter_components_side(&components, "top").unwrap();
        assert_eq!(top.len(), top2.len());

        let bottom2 = filter_components_side(&components, "Bottom").unwrap();
        assert_eq!(bottom.len(), bottom2.len());

        assert!(filter_components_side(&components, "NONEXISTENT").is_err());
    }

    #[test]
    fn test_filter_name() {
        let components = components();

        let filtered = filter_components_name(&components, r"^D\d+$").unwrap();
        assert_eq!(filtered.len(), 80);

        assert!(filter_components_name(&components, r"\").is_err());

        assert!(filter_components_name(&components, r"^NONEXISTENT$").is_err());
    }

    #[test]
    fn test_locate_reference() {
        let components = components();

        assert_eq!(locate_reference(&components, "D1").unwrap(), 20);
        assert_eq!(locate_reference(&components, "D62").unwrap(), 0);
        assert_eq!(locate_reference(&components, "D").unwrap(), 0);
        assert_eq!(locate_reference(&components, "C").unwrap(), 19);
        assert!(locate_reference(&components, "C20").is_err());
    }

    #[test]
    fn test_prepend_reference() {
        let components = components();

        let sorted = prepend_reference(&components, "D1").unwrap();
        assert_eq!(sorted[0].designator, "D1");

        let sorted = prepend_reference(&components, "D").unwrap();
        assert_eq!(sorted[0].designator, "D62");

        assert!(prepend_reference(&components, "D1000").is_err());
    }
}
