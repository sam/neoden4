use super::{Component, Fiducial};
use std::path::Path;

/// Read components positions and characteristics from a tab delimited file.
pub fn read_components<P: AsRef<Path>>(f: P) -> Result<Vec<Component>, csv::Error> {
    let mut r = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b'\t')
        .flexible(true)
        .from_path(f)?;
    Ok(r.deserialize().filter_map(|l| l.ok()).collect())
}

/// Read fiducials positions and characteristics from a tab delimited file.
pub fn read_fiducials<P: AsRef<Path>>(f: P) -> Result<Vec<Fiducial>, csv::Error> {
    let mut r = csv::ReaderBuilder::new()
        .has_headers(false)
        .delimiter(b'\t')
        .flexible(true)
        .from_path(f)?;
    Ok(r.deserialize().filter_map(|l| l.ok()).collect())
}
