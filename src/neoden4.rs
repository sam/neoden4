use super::{Component, Fiducial};
use csv::QuoteStyle;
use std::io::Write;

/// Component to place as represented in the CSV file for the Neoden 4
#[derive(Serialize)]
struct Neoden4Component {
    designator: String,
    footprint: String,
    #[serde(serialize_with = "round3")]
    mid_x: f32,
    #[serde(serialize_with = "round3")]
    mid_y: f32,
    #[serde(serialize_with = "round3")]
    ref_x: f32,
    #[serde(serialize_with = "round3")]
    ref_y: f32,
    #[serde(serialize_with = "round3")]
    pad_x: f32,
    #[serde(serialize_with = "round3")]
    pad_y: f32,
    layer: String,
    #[serde(serialize_with = "round3")]
    rotation: f32,
    comment: String,
}

/// Helper for rounding to three decimals in the output
fn round3<S: serde::Serializer>(x: &f32, s: S) -> Result<S::Ok, S::Error> {
    s.serialize_str(&format!("{:.3}", x))
}

/// Displace a component by an offset.
fn displace(c: &Component, offset_x: f32, offset_y: f32) -> Neoden4Component {
    Neoden4Component {
        designator: c.designator.clone(),
        footprint: c.package.clone(),
        mid_x: c.x + offset_x,
        mid_y: c.y + offset_y,
        ref_x: c.x + offset_x,
        ref_y: c.y + offset_y,
        pad_x: c.x + offset_x,
        pad_y: c.y + offset_y,
        layer: c.side.chars().take(1).collect(),
        rotation: c.rotation,
        comment: c.value.clone(),
    }
}

/// Write the components list using the Neoden 4 CSV format. Components are
/// written in order and translated so that the first component is located
/// at position (ref_x, ref_y). Return an error if the CSV cannot be written.
pub fn write_components<W: Write>(
    f: W,
    components: &[Component],
    ref_x: f32,
    ref_y: f32,
) -> Result<(), csv::Error> {
    let c1 = &components[0];
    let offset_x = ref_x - c1.x;
    let offset_y = ref_y - c1.y;
    let mut w = csv::WriterBuilder::new()
        .flexible(true)
        .has_headers(false)
        .quote_style(QuoteStyle::Always)
        .from_writer(f);
    w.write_record([
        "Designator",
        "Footprint",
        "Mid X",
        "Mid Y",
        "Ref X",
        "Ref Y",
        "Pad X",
        "Pad Y",
        "Layer",
        "Rotation",
        "Comment",
    ])?;
    w.write_record([""])?;
    for c in components {
        w.serialize(&displace(c, offset_x, offset_y))?;
    }
    Ok(())
}

/// Print the fiducials list after applying the same transformation as in
/// [write_components].
pub fn print_fiducials(fiducials: &[Fiducial], components: &[Component], ref_x: f32, ref_y: f32) {
    let c1 = &components[0];
    let offset_x = ref_x - c1.x;
    let offset_y = ref_y - c1.y;
    for f in fiducials {
        println!(
            "Fiducial {} at position ({:.3}, {:.3})",
            f.designator,
            f.x + offset_x,
            f.y + offset_y
        );
    }
}
